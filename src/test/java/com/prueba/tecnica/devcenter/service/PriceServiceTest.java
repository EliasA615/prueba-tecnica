package com.prueba.tecnica.devcenter.service;

import com.prueba.tecnica.devcenter.data.entity.Car;
import com.prueba.tecnica.devcenter.data.entity.Price;
import com.prueba.tecnica.devcenter.data.entity.Trademark;
import com.prueba.tecnica.devcenter.data.repository.PriceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.Optional;

class PriceServiceTest {

    @Autowired
    private PriceRepository priceRepository = Mockito.mock(PriceRepository.class);

    @Autowired
    private PriceService priceService;

    @BeforeEach
    public void setUp() {
        LocalDate date = LocalDate.of(2021,3,10);

        Trademark trademarkMock = new Trademark();
        trademarkMock.setId(1L);
        trademarkMock.setTrademarkName("toyota");

        Car carMock = new Car();
        carMock.setId(1L);
        carMock.setColor("rojo");
        carMock.setModelName("modelo-01");
        carMock.setTrademark(trademarkMock);

        Price priceMock = new Price();
        priceMock.setId(1L);
        priceMock.setCar(carMock);
        priceMock.setEndDate(LocalDate.of(2021, 3, 1));
        priceMock.setEndDate(LocalDate.of(2021, 3, 30));
        priceMock.setCost(1000000.0);

        Mockito.when(priceRepository.findByCarAndDate(1L, date)).thenReturn(Optional.of(priceMock));
    }

    @Test
    public void getPriceTest() {

    }
}