INSERT INTO trademark (trademark_name) VALUES
  ('Toyota'),
  ('BMW'),
  ('Ferrari');

INSERT INTO car (model_name, color, trademark_id) VALUES
  ('1568', 'verde', 1),
  ('1658', 'rojo', 2),
  ('2010', 'negro', 3);

INSERT INTO price (start_date, end_date, cost, car_id) VALUES
  ('2021-03-01', '2021-03-20', 1000000, 1),
  ('2021-04-01', '2021-04-20', 900000.5, 1),
  ('2021-05-01', '2021-05-20', 1003900.9, 2),
  ('2021-06-01', '2021-06-20', 1000400, 2),
  ('2021-07-01', '2021-07-20', 905000.5, 3),
  ('2021-08-01', '2021-08-20', 1200900.9, 3);