package com.prueba.tecnica.devcenter.service;

import com.prueba.tecnica.devcenter.data.entity.ApiCall;
import com.prueba.tecnica.devcenter.data.repository.ApiCallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApiCallService {

    @Autowired
    private ApiCallRepository apiCallRepository;

    public ApiCall save(ApiCall apiCall) {
        return apiCallRepository.insert(apiCall);
    }

}
