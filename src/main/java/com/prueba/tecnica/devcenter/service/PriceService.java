package com.prueba.tecnica.devcenter.service;

import com.prueba.tecnica.devcenter.data.entity.Price;
import com.prueba.tecnica.devcenter.data.repository.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class PriceService {

    @Autowired
    private PriceRepository priceRepository;

    public Price getPrice(Long carId, LocalDate date) {
        return priceRepository.findByCarAndDate(carId, date).orElseThrow();
    }

}
