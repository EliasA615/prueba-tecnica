package com.prueba.tecnica.devcenter.service;

import com.prueba.tecnica.devcenter.data.entity.Car;
import com.prueba.tecnica.devcenter.data.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;

    public List<Car> getAllCars() {
        return carRepository.findAll();
    }

}
