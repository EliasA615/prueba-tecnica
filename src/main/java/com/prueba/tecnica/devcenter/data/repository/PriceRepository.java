package com.prueba.tecnica.devcenter.data.repository;

import com.prueba.tecnica.devcenter.data.entity.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface PriceRepository extends JpaRepository<Price, Long> {

    @Query("select p from Price p where p.car.id = ?1 and p.startDate <= ?2 and p.endDate >= ?2")
    Optional<Price> findByCarAndDate(Long carId, LocalDate date);

}
