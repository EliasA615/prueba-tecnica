package com.prueba.tecnica.devcenter.data.repository;

import com.prueba.tecnica.devcenter.data.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

}
