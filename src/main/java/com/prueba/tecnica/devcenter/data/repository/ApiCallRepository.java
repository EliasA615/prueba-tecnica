package com.prueba.tecnica.devcenter.data.repository;

import com.prueba.tecnica.devcenter.data.entity.ApiCall;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiCallRepository extends MongoRepository<ApiCall, Long> {

}
