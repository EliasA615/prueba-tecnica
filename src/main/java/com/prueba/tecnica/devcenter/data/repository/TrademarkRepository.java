package com.prueba.tecnica.devcenter.data.repository;

import com.prueba.tecnica.devcenter.data.entity.Trademark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrademarkRepository extends JpaRepository<Trademark, Long> {
}
