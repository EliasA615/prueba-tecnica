package com.prueba.tecnica.devcenter.data.entity;


import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;

@Data
@Document(collection = "api_calls")
public class ApiCall {

    private String ip;

    private LocalDateTime date;

    private String service;

}
