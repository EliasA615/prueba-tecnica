package com.prueba.tecnica.devcenter.aspect;

import com.prueba.tecnica.devcenter.data.entity.ApiCall;
import com.prueba.tecnica.devcenter.service.ApiCallService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Aspect
@Component
public class LogAspect {

    @Autowired
    private ApiCallService apiCallService;

    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void springBeanPointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    @Pointcut("within(com.prueba.tecnica.devcenter.*.rest.controller..*)")
    public void applicationPackagePointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    @Before("applicationPackagePointcut() || springBeanPointcut()")
    public void logBefore(JoinPoint joinPoint) {
        ApiCall apiCall = new ApiCall();

        HttpServletRequest http = (HttpServletRequest) joinPoint.getArgs()[1];
        apiCall.setIp(http.getRemoteAddr());
        apiCall.setDate(LocalDateTime.now());
        apiCall.setService(joinPoint.getSignature().toShortString());

        apiCallService.save(apiCall);
    }

    private Logger logger(JoinPoint joinPoint) {
        return LoggerFactory.getLogger(joinPoint.getSignature().getDeclaringTypeName());
    }

}
