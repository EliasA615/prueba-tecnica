package com.prueba.tecnica.devcenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevcenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevcenterApplication.class, args);
	}

}
