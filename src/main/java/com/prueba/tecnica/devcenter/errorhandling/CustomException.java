package com.prueba.tecnica.devcenter.errorhandling;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Builder
@Data
public class CustomException extends RuntimeException {

    private HttpStatus status;
    private String title;
    private String detail;
    private String code;

}
