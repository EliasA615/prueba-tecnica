package com.prueba.tecnica.devcenter.rest.response;

import com.prueba.tecnica.devcenter.data.entity.Price;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter @Setter
public class PriceResponse {

    private Long carId;

    private Long tradeMarkId;

    private Long priceId;

    private LocalDate startDate;

    private LocalDate endDate;

    private double cost;

    public PriceResponse(Price price) {
        setCarId(price.getCar().getId());
        setTradeMarkId(price.getCar().getTrademark().getId());
        setPriceId(price.getId());
        setStartDate(price.getStartDate());
        setEndDate(price.getEndDate());
        setCost(price.getCost());
    }

}
