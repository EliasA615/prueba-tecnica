package com.prueba.tecnica.devcenter.rest.controller;

import com.prueba.tecnica.devcenter.data.entity.Car;
import com.prueba.tecnica.devcenter.service.CarService;
import com.prueba.tecnica.devcenter.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private CarService carService;

    @GetMapping("/cars/excel")
    public void exportToExcel(HttpServletResponse response, HttpServletRequest http) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=cars_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<Car> listUsers = carService.getAllCars();

        ReportService excelExporter = new ReportService(listUsers);

        excelExporter.export(response);
    }

}
