package com.prueba.tecnica.devcenter.rest.controller;

import com.prueba.tecnica.devcenter.rest.request.PriceRequest;
import com.prueba.tecnica.devcenter.rest.response.PriceResponse;
import com.prueba.tecnica.devcenter.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/price")
public class PriceController {

    @Autowired
    private PriceService priceService;

    @GetMapping("/get")
    public PriceResponse getPrice(@RequestBody PriceRequest request, HttpServletRequest httpReq) {
        return new PriceResponse(priceService.getPrice(request.getCarId(), request.getEnforcementDate()));
    }

}
