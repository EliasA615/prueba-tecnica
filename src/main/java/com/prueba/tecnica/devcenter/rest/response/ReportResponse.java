package com.prueba.tecnica.devcenter.rest.response;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class ReportResponse {

    private String status;

    private String description;

    private String path;

    public ReportResponse(String status, String description, String path) {
        this.status = status;
        this.description = description;
        this.path = path;
    }

}
