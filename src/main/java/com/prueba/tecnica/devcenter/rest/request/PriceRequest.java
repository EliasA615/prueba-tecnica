package com.prueba.tecnica.devcenter.rest.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Getter @Setter @ToString
public class PriceRequest {

    private LocalDate enforcementDate;

    private Long carId;

}
